# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import testSwagt.aura.client.invoker.*;
import testSwagt.aura.client.invoker.auth.*;
import testSwagt.aura.client.model.*;
import testSwagt.aura.client.api.ItemApi;

import java.io.File;
import java.util.*;

public class ItemApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        
        // Configure OAuth2 access token for authorization: itemstore_auth
        OAuth itemstore_auth = (OAuth) defaultClient.getAuthentication("itemstore_auth");
        itemstore_auth.setAccessToken("YOUR ACCESS TOKEN");

        ItemApi apiInstance = new ItemApi();
        Item body = new Item(); // Item | Item object that needs to be added to the warehouse
        try {
            apiInstance.additem(body);
        } catch (ApiException e) {
            System.err.println("Exception when calling ItemApi#additem");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://virtserver.swaggerhub.com/vahAAA/AuraCore/1.0.0*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ItemApi* | [**additem**](docs/ItemApi.md#additem) | **POST** /item | Add a new item to the warehouse
*ItemApi* | [**deleteitem**](docs/ItemApi.md#deleteitem) | **DELETE** /item/{itemId} | Deletes a item
*ItemApi* | [**finditemsByName**](docs/ItemApi.md#finditemsByName) | **GET** /item/findByName | Finds items by name
*ItemApi* | [**getItemById**](docs/ItemApi.md#getItemById) | **GET** /item/{itemId} | Find item by ID
*ItemApi* | [**updateItem**](docs/ItemApi.md#updateItem) | **PUT** /item | Update an existing item
*ItemApi* | [**updateItemWithForm**](docs/ItemApi.md#updateItemWithForm) | **POST** /item/{itemId} | Updates a item in the store with form data


## Documentation for Models

 - [CommonItem](docs/CommonItem.md)
 - [ComplexItem](docs/ComplexItem.md)
 - [ComponentItem](docs/ComponentItem.md)
 - [Contacts](docs/Contacts.md)
 - [Department](docs/Department.md)
 - [Item](docs/Item.md)
 - [ItemLog](docs/ItemLog.md)
 - [PrivateItem](docs/PrivateItem.md)
 - [Resource](docs/Resource.md)
 - [TimeResource](docs/TimeResource.md)
 - [UsageAmountResource](docs/UsageAmountResource.md)
 - [User](docs/User.md)
 - [UserCommonItemEvent](docs/UserCommonItemEvent.md)
 - [UserCredentials](docs/UserCredentials.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### api_key

- **Type**: API key
- **API key parameter name**: api_key
- **Location**: HTTP header

### itemstore_auth

- **Type**: OAuth
- **Flow**: implicit
- **Authorizatoin URL**: http://itemstore.swagger.io/oauth/dialog
- **Scopes**: 
  - write:items: modify items in your account
  - read:items: read your items


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

evanvaha@gmail.com

