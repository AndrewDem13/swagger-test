
# CommonItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**isWorking** | **Boolean** |  |  [optional]
**manufactorer** | **String** |  |  [optional]
**model** | **String** |  |  [optional]
**resource** | [**Resource**](Resource.md) |  |  [optional]
**itemLogs** | [**ItemLog**](ItemLog.md) |  |  [optional]
**startPrice** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**currentPrice** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**usagePercentage** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**department** | [**Department**](Department.md) |  |  [optional]
**commonEvents** | [**List&lt;UserCommonItemEvent&gt;**](UserCommonItemEvent.md) |  |  [optional]



