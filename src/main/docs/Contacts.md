
# Contacts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**adresses** | **List&lt;String&gt;** |  |  [optional]
**phones** | **List&lt;String&gt;** |  |  [optional]
**emails** | **List&lt;String&gt;** |  |  [optional]



