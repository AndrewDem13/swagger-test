
# Department

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**workers** | [**List&lt;User&gt;**](User.md) |  |  [optional]
**contacts** | [**Contacts**](Contacts.md) |  |  [optional]



