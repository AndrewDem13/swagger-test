# ItemApi

All URIs are relative to *https://virtserver.swaggerhub.com/vahAAA/AuraCore/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**additem**](ItemApi.md#additem) | **POST** /item | Add a new item to the warehouse
[**deleteitem**](ItemApi.md#deleteitem) | **DELETE** /item/{itemId} | Deletes a item
[**finditemsByName**](ItemApi.md#finditemsByName) | **GET** /item/findByName | Finds items by name
[**getItemById**](ItemApi.md#getItemById) | **GET** /item/{itemId} | Find item by ID
[**updateItem**](ItemApi.md#updateItem) | **PUT** /item | Update an existing item
[**updateItemWithForm**](ItemApi.md#updateItemWithForm) | **POST** /item/{itemId} | Updates a item in the store with form data


<a name="additem"></a>
# **additem**
> additem(body)

Add a new item to the warehouse

### Example
```java
// Import classes:
//import testSwagt.aura.client.invoker.ApiClient;
//import testSwagt.aura.client.invoker.ApiException;
//import testSwagt.aura.client.invoker.Configuration;
//import testSwagt.aura.client.invoker.auth.*;
//import testSwagt.aura.client.api.ItemApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: itemstore_auth
OAuth itemstore_auth = (OAuth) defaultClient.getAuthentication("itemstore_auth");
itemstore_auth.setAccessToken("YOUR ACCESS TOKEN");

ItemApi apiInstance = new ItemApi();
Item body = new Item(); // Item | Item object that needs to be added to the warehouse
try {
    apiInstance.additem(body);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemApi#additem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Item**](Item.md)| Item object that needs to be added to the warehouse |

### Return type

null (empty response body)

### Authorization

[itemstore_auth](../README.md#itemstore_auth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="deleteitem"></a>
# **deleteitem**
> deleteitem(itemId, apiKey)

Deletes a item

### Example
```java
// Import classes:
//import testSwagt.aura.client.invoker.ApiClient;
//import testSwagt.aura.client.invoker.ApiException;
//import testSwagt.aura.client.invoker.Configuration;
//import testSwagt.aura.client.invoker.auth.*;
//import testSwagt.aura.client.api.ItemApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: itemstore_auth
OAuth itemstore_auth = (OAuth) defaultClient.getAuthentication("itemstore_auth");
itemstore_auth.setAccessToken("YOUR ACCESS TOKEN");

ItemApi apiInstance = new ItemApi();
Long itemId = 789L; // Long | item id to delete
String apiKey = "apiKey_example"; // String | 
try {
    apiInstance.deleteitem(itemId, apiKey);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemApi#deleteitem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **Long**| item id to delete |
 **apiKey** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[itemstore_auth](../README.md#itemstore_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="finditemsByName"></a>
# **finditemsByName**
> List&lt;Item&gt; finditemsByName(name)

Finds items by name

Multiple names can be provided with comma separated strings

### Example
```java
// Import classes:
//import testSwagt.aura.client.invoker.ApiClient;
//import testSwagt.aura.client.invoker.ApiException;
//import testSwagt.aura.client.invoker.Configuration;
//import testSwagt.aura.client.invoker.auth.*;
//import testSwagt.aura.client.api.ItemApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: itemstore_auth
OAuth itemstore_auth = (OAuth) defaultClient.getAuthentication("itemstore_auth");
itemstore_auth.setAccessToken("YOUR ACCESS TOKEN");

ItemApi apiInstance = new ItemApi();
List<String> name = Arrays.asList("name_example"); // List<String> | Names that need to be considered for filter
try {
    List<Item> result = apiInstance.finditemsByName(name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemApi#finditemsByName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | [**List&lt;String&gt;**](String.md)| Names that need to be considered for filter |

### Return type

[**List&lt;Item&gt;**](Item.md)

### Authorization

[itemstore_auth](../README.md#itemstore_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getItemById"></a>
# **getItemById**
> Item getItemById(itemId)

Find item by ID

Returns a single item

### Example
```java
// Import classes:
//import testSwagt.aura.client.invoker.ApiClient;
//import testSwagt.aura.client.invoker.ApiException;
//import testSwagt.aura.client.invoker.Configuration;
//import testSwagt.aura.client.invoker.auth.*;
//import testSwagt.aura.client.api.ItemApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: api_key
ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
api_key.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//api_key.setApiKeyPrefix("Token");

ItemApi apiInstance = new ItemApi();
Long itemId = 789L; // Long | ID of item to return
try {
    Item result = apiInstance.getItemById(itemId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemApi#getItemById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **Long**| ID of item to return |

### Return type

[**Item**](Item.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="updateItem"></a>
# **updateItem**
> updateItem(body)

Update an existing item

### Example
```java
// Import classes:
//import testSwagt.aura.client.invoker.ApiClient;
//import testSwagt.aura.client.invoker.ApiException;
//import testSwagt.aura.client.invoker.Configuration;
//import testSwagt.aura.client.invoker.auth.*;
//import testSwagt.aura.client.api.ItemApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: itemstore_auth
OAuth itemstore_auth = (OAuth) defaultClient.getAuthentication("itemstore_auth");
itemstore_auth.setAccessToken("YOUR ACCESS TOKEN");

ItemApi apiInstance = new ItemApi();
Item body = new Item(); // Item | Item object that needs to be updated in the warehouse
try {
    apiInstance.updateItem(body);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemApi#updateItem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Item**](Item.md)| Item object that needs to be updated in the warehouse |

### Return type

null (empty response body)

### Authorization

[itemstore_auth](../README.md#itemstore_auth)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="updateItemWithForm"></a>
# **updateItemWithForm**
> updateItemWithForm(itemId, name)

Updates a item in the store with form data

### Example
```java
// Import classes:
//import testSwagt.aura.client.invoker.ApiClient;
//import testSwagt.aura.client.invoker.ApiException;
//import testSwagt.aura.client.invoker.Configuration;
//import testSwagt.aura.client.invoker.auth.*;
//import testSwagt.aura.client.api.ItemApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: itemstore_auth
OAuth itemstore_auth = (OAuth) defaultClient.getAuthentication("itemstore_auth");
itemstore_auth.setAccessToken("YOUR ACCESS TOKEN");

ItemApi apiInstance = new ItemApi();
Long itemId = 789L; // Long | ID of item that needs to be updated
String name = "name_example"; // String | Updated name of the item
try {
    apiInstance.updateItemWithForm(itemId, name);
} catch (ApiException e) {
    System.err.println("Exception when calling ItemApi#updateItemWithForm");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **Long**| ID of item that needs to be updated |
 **name** | **String**| Updated name of the item | [optional]

### Return type

null (empty response body)

### Authorization

[itemstore_auth](../README.md#itemstore_auth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json, application/xml

