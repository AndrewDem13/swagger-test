
# ItemLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**item** | [**Item**](Item.md) |  |  [optional]
**eventData** | **Integer** |  |  [optional]
**eventType** | [**EventTypeEnum**](#EventTypeEnum) | an event that happened to the item |  [optional]
**eDescription** | **String** |  |  [optional]


<a name="EventTypeEnum"></a>
## Enum: EventTypeEnum
Name | Value
---- | -----
ITEM_CREATED | &quot;ITEM_CREATED&quot;
ITEM_BINDED_TO_USER | &quot;ITEM_BINDED_TO_USER&quot;
ITEM_BROKEN | &quot;ITEM_BROKEN&quot;
ITEM_UPGRADED | &quot;ITEM_UPGRADED&quot;
ITEM_REPAIRED | &quot;ITEM_REPAIRED&quot;
ITEM_RESOURCE_EXPIRED | &quot;ITEM_RESOURCE_EXPIRED&quot;
ITEM_RESOURCE_EXTENDED | &quot;ITEM_RESOURCE_EXTENDED&quot;
OWNER_CHANGED | &quot;OWNER_CHANGED&quot;



