
# TimeResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**timeResource** | **Integer** |  |  [optional]
**workedTime** | **Integer** |  |  [optional]



