
# UsageAmountResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**availableAmount** | **Integer** |  |  [optional]
**usedAmount** | **Integer** |  |  [optional]



