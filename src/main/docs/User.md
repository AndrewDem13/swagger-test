
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**contacts** | [**Resource**](Resource.md) |  |  [optional]
**department** | [**Department**](Department.md) |  |  [optional]
**userItems** | [**List&lt;PrivateItem&gt;**](PrivateItem.md) |  |  [optional]



