
# UserCommonItemEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
**item** | [**Item**](Item.md) |  |  [optional]
**usedPercents** | [**BigDecimal**](BigDecimal.md) |  |  [optional]



