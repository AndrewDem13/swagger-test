
# UserCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**User**](User.md) |  |  [optional]
**login** | **String** |  |  [optional]
**pass** | **String** |  |  [optional]
**role** | [**RoleEnum**](#RoleEnum) | Role of the user |  [optional]


<a name="RoleEnum"></a>
## Enum: RoleEnum
Name | Value
---- | -----
SUPERADMIN | &quot;SUPERADMIN&quot;
ADMIN | &quot;ADMIN&quot;
MANAGER | &quot;MANAGER&quot;
USER | &quot;USER&quot;



