/*
 * AuraAPI
 * This is a simple API for monitoring IT company's warehouse 
 *
 * OpenAPI spec version: 1.0.0
 * Contact: evanvaha@gmail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package Swagger_test.aura.client.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import testSwagt.aura.client.model.Department;
import testSwagt.aura.client.model.Item;
import testSwagt.aura.client.model.ItemLog;
import testSwagt.aura.client.model.Resource;
import testSwagt.aura.client.model.UserCommonItemEvent;

/**
 * CommonItem
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-14T17:24:53.333+03:00")
public class CommonItem {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("isWorking")
  private Boolean isWorking = null;

  @SerializedName("manufactorer")
  private String manufactorer = null;

  @SerializedName("model")
  private String model = null;

  @SerializedName("resource")
  private Resource resource = null;

  @SerializedName("itemLogs")
  private ItemLog itemLogs = null;

  @SerializedName("start_price")
  private BigDecimal startPrice = null;

  @SerializedName("current_price")
  private BigDecimal currentPrice = null;

  @SerializedName("usagePercentage")
  private BigDecimal usagePercentage = null;

  @SerializedName("department")
  private Department department = null;

  @SerializedName("commonEvents")
  private List<UserCommonItemEvent> commonEvents = new ArrayList<UserCommonItemEvent>();

  public CommonItem id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "null", required = true, value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public CommonItem isWorking(Boolean isWorking) {
    this.isWorking = isWorking;
    return this;
  }

   /**
   * Get isWorking
   * @return isWorking
  **/
  @ApiModelProperty(example = "null", value = "")
  public Boolean getIsWorking() {
    return isWorking;
  }

  public void setIsWorking(Boolean isWorking) {
    this.isWorking = isWorking;
  }

  public CommonItem manufactorer(String manufactorer) {
    this.manufactorer = manufactorer;
    return this;
  }

   /**
   * Get manufactorer
   * @return manufactorer
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getManufactorer() {
    return manufactorer;
  }

  public void setManufactorer(String manufactorer) {
    this.manufactorer = manufactorer;
  }

  public CommonItem model(String model) {
    this.model = model;
    return this;
  }

   /**
   * Get model
   * @return model
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public CommonItem resource(Resource resource) {
    this.resource = resource;
    return this;
  }

   /**
   * Get resource
   * @return resource
  **/
  @ApiModelProperty(example = "null", value = "")
  public Resource getResource() {
    return resource;
  }

  public void setResource(Resource resource) {
    this.resource = resource;
  }

  public CommonItem itemLogs(ItemLog itemLogs) {
    this.itemLogs = itemLogs;
    return this;
  }

   /**
   * Get itemLogs
   * @return itemLogs
  **/
  @ApiModelProperty(example = "null", value = "")
  public ItemLog getItemLogs() {
    return itemLogs;
  }

  public void setItemLogs(ItemLog itemLogs) {
    this.itemLogs = itemLogs;
  }

  public CommonItem startPrice(BigDecimal startPrice) {
    this.startPrice = startPrice;
    return this;
  }

   /**
   * Get startPrice
   * @return startPrice
  **/
  @ApiModelProperty(example = "null", value = "")
  public BigDecimal getStartPrice() {
    return startPrice;
  }

  public void setStartPrice(BigDecimal startPrice) {
    this.startPrice = startPrice;
  }

  public CommonItem currentPrice(BigDecimal currentPrice) {
    this.currentPrice = currentPrice;
    return this;
  }

   /**
   * Get currentPrice
   * @return currentPrice
  **/
  @ApiModelProperty(example = "null", value = "")
  public BigDecimal getCurrentPrice() {
    return currentPrice;
  }

  public void setCurrentPrice(BigDecimal currentPrice) {
    this.currentPrice = currentPrice;
  }

  public CommonItem usagePercentage(BigDecimal usagePercentage) {
    this.usagePercentage = usagePercentage;
    return this;
  }

   /**
   * Get usagePercentage
   * @return usagePercentage
  **/
  @ApiModelProperty(example = "null", value = "")
  public BigDecimal getUsagePercentage() {
    return usagePercentage;
  }

  public void setUsagePercentage(BigDecimal usagePercentage) {
    this.usagePercentage = usagePercentage;
  }

  public CommonItem department(Department department) {
    this.department = department;
    return this;
  }

   /**
   * Get department
   * @return department
  **/
  @ApiModelProperty(example = "null", value = "")
  public Department getDepartment() {
    return department;
  }

  public void setDepartment(Department department) {
    this.department = department;
  }

  public CommonItem commonEvents(List<UserCommonItemEvent> commonEvents) {
    this.commonEvents = commonEvents;
    return this;
  }

  public CommonItem addCommonEventsItem(UserCommonItemEvent commonEventsItem) {
    this.commonEvents.add(commonEventsItem);
    return this;
  }

   /**
   * Get commonEvents
   * @return commonEvents
  **/
  @ApiModelProperty(example = "null", value = "")
  public List<UserCommonItemEvent> getCommonEvents() {
    return commonEvents;
  }

  public void setCommonEvents(List<UserCommonItemEvent> commonEvents) {
    this.commonEvents = commonEvents;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommonItem commonItem = (CommonItem) o;
    return Objects.equals(this.id, commonItem.id) &&
        Objects.equals(this.isWorking, commonItem.isWorking) &&
        Objects.equals(this.manufactorer, commonItem.manufactorer) &&
        Objects.equals(this.model, commonItem.model) &&
        Objects.equals(this.resource, commonItem.resource) &&
        Objects.equals(this.itemLogs, commonItem.itemLogs) &&
        Objects.equals(this.startPrice, commonItem.startPrice) &&
        Objects.equals(this.currentPrice, commonItem.currentPrice) &&
        Objects.equals(this.usagePercentage, commonItem.usagePercentage) &&
        Objects.equals(this.department, commonItem.department) &&
        Objects.equals(this.commonEvents, commonItem.commonEvents);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, isWorking, manufactorer, model, resource, itemLogs, startPrice, currentPrice, usagePercentage, department, commonEvents);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommonItem {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    isWorking: ").append(toIndentedString(isWorking)).append("\n");
    sb.append("    manufactorer: ").append(toIndentedString(manufactorer)).append("\n");
    sb.append("    model: ").append(toIndentedString(model)).append("\n");
    sb.append("    resource: ").append(toIndentedString(resource)).append("\n");
    sb.append("    itemLogs: ").append(toIndentedString(itemLogs)).append("\n");
    sb.append("    startPrice: ").append(toIndentedString(startPrice)).append("\n");
    sb.append("    currentPrice: ").append(toIndentedString(currentPrice)).append("\n");
    sb.append("    usagePercentage: ").append(toIndentedString(usagePercentage)).append("\n");
    sb.append("    department: ").append(toIndentedString(department)).append("\n");
    sb.append("    commonEvents: ").append(toIndentedString(commonEvents)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

